import sys
import logging
import click
import os
from .m import whittaker_filtering
from osgeo import gdal
logging.basicConfig(
    stream=sys.stderr,
    level=logging.DEBUG,
    format="%(asctime)s %(levelname)-8s %(message)s",
    datefmt="%Y-%m-%dT%H:%M:%S",
)


@click.command(
    short_help="Whittaker Smoothing and Gap Filling",
    help="Whittaker Smoothing and Gap Filling",
    context_settings=dict(
        ignore_unknown_options=True,
        allow_extra_args=True,
    ),
)
@click.option(
    "--input_path",
    "-i",
    "input_path",
    help="An input reference",
    type=click.Path(),
    required=True,
)
@click.option(
    "--delta_day",
    "-d",
    "delta_day_interval",
    help="The interval between interpolated-smoothed output files - integer value",
    required=True,
)
@click.option(
    "--no_data",
    "-nd",
    "no_data_value",
    help="what nodata value your input tifs have?",
    required=True,
)
@click.pass_context
def main(ctx, input_path,delta_day_interval,no_data_value):
 
    
    
    logging.info("Begining of smoothing & gap-filling process...")
    logging.info("Input directory is: {}".format(input_path))

    input_items =[os.path.basename(x) for x in os.listdir(input_path) if x.split('.')[-1]=='tif']

    logging.info(
        "Items tif: {}".format(input_items)
    )

    whittaker_filtering(input_path,int(delta_day_interval),int(no_data_value))

    sys.exit(0)


if __name__ == "__main__":
    main()





