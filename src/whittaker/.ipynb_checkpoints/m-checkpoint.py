from osgeo import gdal
import array

import os
import sys
import logging
import numpy as np

import osr

import pandas as pd
import datetime

from vam.whittaker import ws2d, ws2doptv, ws2doptvp, lag1corr

import array
import geopandas as gpd


logging.basicConfig(
    stream=sys.stderr,
    level=logging.DEBUG,
    format="%(asctime)s %(levelname)-8s %(message)s",
    datefmt="%Y-%m-%dT%H:%M:%S",
)


def analyse_row(row):
    
    series = dict()

    series['day'] = row['item'][10:18]
    series['jday'] = '{}{}'.format(datetime.datetime.strptime(series['day'], '%Y%m%d').timetuple().tm_year,
                                   "%03d"%datetime.datetime.strptime(series['day'], '%Y%m%d').timetuple().tm_yday)
    
    
    return pd.Series(series)

def fromjulian(x):
    """
    Parses julian date string to datetime object.

    Args:
        x: julian date as string YYYYJJJ

    Returns:
        datetime object parsed from julian date
    """

    return datetime.datetime.strptime(x, '%Y%j').date()


    
def generate_dates(startdate_string=None, enddate_string=None, delta=5):
    """
    Generates a list of dates from a start date to an end date.

    Args:
        startdate_string: julian date as string YYYYJJJ
        enddate_string: julian date as string YYYYJJJ
        delta: integer timedelta between each date

    Returns:
        list of string julian dates YYYYJJJ
    """

    
    startdate = datetime.datetime.strptime(startdate_string, '%Y%j').date()
    enddate = datetime.datetime.strptime(enddate_string, '%Y%j').date()
    
    date_generated = [startdate + datetime.timedelta(days=x) for x in range(0, (enddate-startdate).days+delta, delta)]
    
    datelist = ['{}{:03d}'.format(x.year, x.timetuple().tm_yday) for x in date_generated]

    return datelist


def whittaker(ts, date_mask):
    """
    Apply the whittaker smoothing to a 1d array of floating values.
    Args:
        ts: array of floating values
        date_mask: full list of julian dates as string YYYYJJJ
    Returns:
        list of floating values. The first value is the s smoothing parameter
    """
    nan_value = -2

        
    ts_double=np.array(ts,dtype='double')
    mask = np.ones(len(ts))
    mask[ts==nan_value]=0
    # the output is an  array full of np.nan by default
    data_smooth = np.array([nan_value]*len(date_mask))
    
    # check if all values are np.npn
    if np.sum(mask)>0:

        w=np.array((ts!=nan_value)*1,dtype='double')
        lrange = array.array('d', np.linspace(-2, 4, 61))
        
        try: 
            # apply whittaker filter with V-curve
            zv, loptv = ws2doptvp(ts_double, w, lrange, p=0.90)
            #parameters needed for the interpolation step
           
            dvec = np.zeros(len(date_mask))
            w_d=np.ones(len(date_mask), dtype='double')

            
            # adding new dates with no associated product to the weights
            for idx, el in enumerate(date_mask):
                if not el:
                    w_d[idx]= 0

            dvec[w_d==1]= zv
            
            # apply whittaker filter with very low smoothing to interpolate
            data_smooth = ws2d(dvec, 0.0001, w_d)
            
            # Calculates Lag-1 correlation
            
            lag1 = lag1corr(ts_double[:-1], ts_double[1:], nan_value)
            
            


        except Exception as e:
            loptv = 0
            lag1 = nan_value
            print(e)
            print(mask)

    else:
        loptv = 0
        lag1 = nan_value
        

    return tuple(np.append(np.append(loptv,lag1), data_smooth))



def whittaker_filtering(input_path,delta_day):
    input_items =[os.path.basename(x) for x in os.listdir(input_path) if x.split('.')[-1]=='tif']
    for tif_item in input_items:
        
        logging.info("Item: {}".format(tif_item))
        item=os.path.join(input_path,tif_item)
        ds = gdal.Open(item)
        logging.info("size is : {}x{}".format(ds.RasterXSize, ds.RasterYSize))
        logging.info("Date is : {}".format(tif_item[10:18]))
    block_size = (1, 1, 256, 512)
    X = ds.RasterXSize  # cols
    Y = ds.RasterYSize  # rows
    transform = ds.GetGeoTransform()

    xOrigin = transform[0]
    yOrigin = transform[3]
    pixelWidth = transform[1]
    pixelHeight = -transform[5]

    logging.info("XRaster - cols is {}".format(X))

    logging.info("YRaster - rows is {}".format(Y))

    # if X < array_size[3] or Y < array_size[2]:
    #     print("Error: subset is bigger than the image extents.")
    #     raise AssertionError()



    filename = 'hello.tif'
    dt = gdal.GDT_Float32  


    df = pd.DataFrame (input_items, columns = ['item'])

    df = df.merge(df.apply(lambda row: analyse_row(row), axis=1),
                                                        left_index=True,
                                                        right_index=True)

    sorted_df = df.sort_values(by=['jday'], ascending=True).reset_index(drop=True) 
    df=None
    dates = sorted_df['jday']
    full_dates = generate_dates(startdate_string=list(dates)[0], enddate_string=list(dates)[-1], delta=1)
    full_period = '{}_{}'.format(fromjulian(full_dates[0]).strftime('%Y%m%d'),fromjulian(full_dates[-1]).strftime('%Y%m%d'))
    logging.info("full_period is : {}".format(full_period))
    date_mask = np.array([True if x in list(dates) else False for x in full_dates])
    logging.info("length date_mask is : {}".format(date_mask.shape))
    data = []
    for index,row in sorted_df.iterrows():
        src_ds = gdal.Open(os.path.join(input_path,row['item']))
        data.append(src_ds.GetRasterBand(1).ReadAsArray())
    data_stack = np.stack(data,axis=0)
    logging.info("shape of data_stack is : {}".format(data_stack.shape))
    logging.info("Whittaker filtering....\n please wait...\n")
    result = np.apply_along_axis(whittaker, 0, data_stack,date_mask)
    logging.info("shape of result is : {}".format(result.shape))
    #delta_day_index=0
    t_values = []
    dataq = []
    out_bands = range(0, result.shape[0]-2, delta_day)
    for index in out_bands:
            product_date = datetime.datetime.strptime(full_dates[index], '%Y%j').date()
            
            band_is_interpolated = 0 if full_dates[index] in list(dates) else 1
            
            date = int('{}{:02d}{:02d}{}'.format(product_date.year, product_date.month, product_date.day,band_is_interpolated))
            print(date)
            t_values.append(date)
            dataq.append(result[index + 2])
    x = np.stack(dataq)

    logging.info("shape of data to be written : {}".format(x.shape))
    array_size = (len(t_values), 1, ds.RasterYSize, ds.RasterXSize)
    #coordinate X , Y
    idx_x = []
    idx_y = []
    for i in range(array_size[3]):
        idx_x.append(((i + 0.5) * pixelWidth) + xOrigin)

    for j in range(array_size[2]):
        idx_y.append(((j + 0.5) * pixelHeight) + yOrigin)


    def write():
        ds = gdal.GetDriverByName("GTiff").CreateMultiDimensional(filename)
        rg = ds.GetRootGroup()

        dimT = rg.CreateDimension("Time", "TEMPORAL", None, array_size[0])
        dimZ = rg.CreateDimension("NBR", "d", None, array_size[1])
        dimY = rg.CreateDimension("Latitude", "HORIZONTAL_Y", None, array_size[2])
        dimX = rg.CreateDimension("Longitude", "HORIZONTAL_X", None, array_size[3])

        # print(len(t_values),arraySize[0])
        assert len(dates) == array_size[0]
        dimTVar = rg.CreateMDArray(
            "Time",
            [dimT],
            gdal.ExtendedDataType.Create(gdal.GDT_Int32),
            ["IS_INDEXING_VARIABLE=YES"],
        )
        dimT.SetIndexingVariable(dimTVar)
        dimTVar.Write(array.array("f", t_values))

        assert len(idx_x) == array_size[3]
        dimXVar = rg.CreateMDArray(
            "Lon",
            [dimX],
            gdal.ExtendedDataType.Create(gdal.GDT_Int32),
            ["IS_INDEXING_VARIABLE=YES"],
        )
        dimX.SetIndexingVariable(dimXVar)
        dimXVar.Write(array.array("f", idx_x))

        assert len(idx_y) == array_size[2]
        dimYVar = rg.CreateMDArray(
            "Lat",
            [dimY],
            gdal.ExtendedDataType.Create(gdal.GDT_Int32),
            ["IS_INDEXING_VARIABLE=YES"],
        )
        dimY.SetIndexingVariable(dimYVar)
        dimYVar.Write(array.array("f", idx_y))

        ar = rg.CreateMDArray(
            "myarray",
            [dimT, dimZ, dimY, dimX],
            gdal.ExtendedDataType.Create(dt),
            [
                "BLOCKSIZE=%d,%d,%d,%d"
                % (block_size[0], block_size[1], block_size[2], block_size[3])
            ],
        )

        numpy_ar = np.reshape(
            x, (dimT.GetSize(), dimZ.GetSize(), dimY.GetSize(), dimX.GetSize()))

        assert ar.Write(numpy_ar) == gdal.CE_None

        print(ar.GetTotalElementsCount())
        print(ar.GetDimensionCount())

        return numpy_ar

    ref_ar = write()
